package com.itai.triads;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Paint;

import java.util.ArrayList;
import java.util.Random;

public class Board {

    //These settings are the boards default
    private static final Board ourInstance = new Board(3, 4, 40, 20, 50);

    Context context = null;

    //Yes, I'm using ArrayList. Sue me.
    private ArrayList<Card> availableCards;
    private ArrayList<Card> activeCards;
    private int sizeX;
    private int sizeY;
    private boolean setup = false;

    private Selection selection;
    private Random random;
    private PlayerT playerT;
    private boolean gameOver;

    private int offsetX;
    private int offsetY;
    //The ammount of space between each card
    private int space;


    public static Board getInstance() {return ourInstance;}

    private Board(int sizeX, int sizeY, int offsetX, int offsetY, int space) {
        selection = Selection.getInstance();
        random = new Random();
        this.sizeX = sizeX;
        this.sizeY = sizeY;
        this.offsetX = offsetX;
        this.offsetY = offsetY;
        this.space = space;
        gameOver = false;

        availableCards = new ArrayList<Card>(81);
        activeCards = new ArrayList<Card>(sizeY*sizeX);
    }

    public ArrayList<Card> getAvailableCards() {
        return availableCards;
    }
    public ArrayList<Card> getActiveCards() {
        return activeCards;
    }

    public void setOffsetY(int offsetY) {
        this.offsetY = offsetY;
    }
    public int getOffsetY() {
        return offsetY;
    }
    public int getOffsetX() {
        return offsetX;
    }
    public void setSpace(int space) { this.space = space; }
    public int getSpace() {return space;}
    public void spaceRatio(double ratio) {space *= ratio;}

    public boolean isGameOver() { return gameOver; }
    public boolean isSetup() { return setup; }


    /*
    As far as I can tell, the context needs to be added externally.
    We can only generate the cards after this is done.
    It will be made sure the game view class (from where the context will be derived) that the context is provided in the
    constructor, which will be the first thing run.
    In a way, this is the true constructor of this class. Singletons are weird. Or I'm doing it wrong. Probably both.
     */
    public void setContext(Context context) {
        if (this.context == null){ //Just for safety. (Even though I didn't put any in selection lol)
            this.context = context;
            generateCards();
        }
    }

    public void setPlayerT(PlayerT playerT) {
        this.playerT = playerT;
    }

    /*
    This method gets called when another player had found a triad.
    It will replace that triad with the same cards that was replaced for that player
    It will also set off motion cards to clearly show us what set what found and by which player
     */
    public void multiReplace(byte[] data, int desX, int desY) {
        MotionCard[] motionCards = new MotionCard[3];
        for (int i = 0; i < 3; i++) {
            //The first two values we don't care about here
            //And then it alternates between the index of the card we want to replace and the index of the card we're replacing it with
            Card card = availableCards.get((int)data[2*i+3]);
            card.setImage();

            int activeIndex = (int) data[2*i+2];

            int cardPosX = (activeIndex % sizeX) * (card.getWidth()  + space) + offsetX;
            int cardPosY = (activeIndex / sizeX) * (card.getHeight() + space) + offsetY;

            motionCards[i] = new MotionCard(cardPosX, cardPosY, desX, desY, card.getImage());

            activeCards.set(activeIndex, card);
            availableCards.remove(card);
        }
        MotionCard.addNew(motionCards);
    }

    /*
    Auxiliary method for createCard()
     */
    private void generateCards() {
        generateCards(0, 0, 0, 0);
    }
    /*
    Recursively generate all cards with all legal values for all properties
     */
    private void generateCards(int number, int shape, int colour, int fill) {
        for (number = 0; number < 3; number++) {
            for (shape = 0; shape < 3; shape++) {
                for (colour = 0; colour < 3; colour++) {
                    for (fill = 0; fill < 3; fill++) {
                        availableCards.add(new Card(context, number, shape, colour, fill));
                    }
                }
            }
        }
    }

    /*
    Populate a grid with cards such that there will always be a valid triad
     */
    public int[] setupBoard() {
        int[] r = new int[sizeX*sizeY];
        for (int i = 0; i < r.length-1; i++) {
            r[i] = addCard();
        }
        r[r.length-1] = addLastCard();
        //Collections.shuffle(activeCards); TODO make this a thing
        setup = true;
        return r;
    }

    public void setupBoard(byte[] message) {
        for (int i = 1; i < message.length; i++) {
            Card card = availableCards.get((int)message[i]);
            if (context != null) card.setImage();
            activeCards.add(card);
            availableCards.remove(card);
        }
        setup = true;
    }

    /*
    Adds a card from availbile cards into active cards at a random index
    Returns the index of the card in available cards
     */
    private int addCard() {
        int chosenIndex = random.nextInt(availableCards.size());
        Card card = availableCards.get(chosenIndex);
        card.setImage();
        activeCards.add(card);
        availableCards.remove(chosenIndex);
        return chosenIndex;
    }

    private int addLastCard() {
        Card card1 = activeCards.get(0);
        Card card2 = activeCards.get(1);
        Card card3 = completeSet(card1, card2);
        int r;
        if(card3 == null) { throw new NullPointerException("Failed to find complete set"); }

        if (!activeCards.contains(card3)) {
            r = availableCards.indexOf(card3);
            card3.setImage();
            activeCards.add(card3);
            availableCards.remove(card3);
        }
        else {
            r = addCard();
        }
        return r;
    }

    private Card completeSet(Card card1, Card card2) {
        Card card3 = new Card (null,(3 - ((card1.getNumber()+card2.getNumber()) % 3)) % 3, (3 - ((card1.getShape()+card2.getShape()) % 3)) % 3, (3 - ((card1.getColour()+card2.getColour()) % 3)) % 3, (3 - ((card1.getFill()+card2.getFill()) % 3)) % 3);
        ArrayList<Card> allCards = new ArrayList<Card>(availableCards);
        allCards.addAll(activeCards);

        for (Card active: allCards) {
            if (active.equals(card3)) {
                return active;
            }
        }
        return null;
    }

    /*
    Replaces a card in active cards with a random card from availble cards at the given index
     */
    private int replaceCards(int i) {
        int r = random.nextInt(availableCards.size());
        Card card = availableCards.get(r);
        if (context != null)card.setImage();
        activeCards.set(i, card);
        availableCards.remove(r);
        return r;
    }

    private int replaceGara(int i) {
        int c = 0;
        while (c <= 100) {
            int[] indices = indexUnselected(activeCards);
            int r1 = indices[0];
            int r2 = indices[1];
            //Log.d("Card test", "Chosen cards: "+activeCards.get(r1).toString()+" "+activeCards.get(r2).toString());

            Card add = completeSet(activeCards.get(r1), activeCards.get(r2));
            if (add != null && availableCards.contains(add)) { //add is null when it's already been removed from the board.
                int r = availableCards.indexOf(add);
                //Log.d("Add card",add.toString());
                if (context!= null) add.setImage();
                availableCards.add(activeCards.get(i));
                availableCards.remove(add);
                activeCards.set(i, add);
                return r;
            }
            c++;
            //Log.d("lol", ""+c);
        }
        gameOver = true;
        return 0; //Since now we set game over to true, it shouldn't matter what is returned here as long as it's valid
    }

    private int[] indexUnselected(ArrayList<Card> cardList) {
        Selection selection = Selection.getInstance();

        int r1 = random.nextInt(cardList.size());
        while (selection.contains(cardList.get(r1))) {
            r1 = random.nextInt(cardList.size());
        }
        int r2 = random.nextInt(cardList.size());
        while (selection.contains(cardList.get(r2)) || r2 == r1) {
            r2 = random.nextInt(cardList.size());
        }

        return new int[]{r1, r2};
    }

    public void drawBoard(Canvas canvas) {
        if (!setup) return; //If we haven't setup the board in the first place, we can't draw it
        for (int r = 0; r < sizeX; r++) {
            for (int c = 0; c < sizeY; c++) {
                Card card = activeCards.get((sizeX)*c+r);
                canvas.drawBitmap(card.getImage(), r * (card.getWidth() + space) + offsetX, c * (card.getHeight() + space) + offsetY, new Paint());
            }
        }
    }


    public boolean touch(float x, float y) {
        for (int r = 0; r < sizeX; r++) {
            for (int c = 0; c < sizeY; c++) {
                Card card = activeCards.get((sizeX)*c+r);
                int pointX = r * (card.getWidth() + space) + offsetX;
                int pointY = c * (card.getHeight() + space) + offsetY;

                //If the touch was within this card
                if (x > pointX && x < pointX + card.getWidth() && y > pointY && y < pointY + card.getHeight()) {
                    if (playerT != null) card.touch(new MotionCard(pointX, pointY, (int) playerT.getX(), (int) playerT.getY(), card.getImage()));
                    else card.touch(null); //For testing
                    if(card.isSelected()) {
                        if (select(card)) {
                            return true;
                        }
                    }
                    else  { //If the card was just touched and it is currently not selected, that means we just deselected it
                        if (!selection.isEmpty()) {
                            deselect(card);
                        }
                    }
                }
            }
        }
        return false;
    }

    private boolean select(Card card) {
        boolean r = false;
        selection.addCard(card);
        if (selection.isFull()) {
            if (selection.isTriad()) {
                MotionCard.addNew(selection.getCard1().getMotionCard(), selection.getCard2().getMotionCard(), selection.getCard3().getMotionCard());
                replaceSelection();
                if (playerT != null) playerT.incrementScore();
                r = true;
            }
            selection.removeAll();
        }
        return r;
    }

    private void deselect(Card card) {
        selection.removeCard(card);
    }

    private void replaceSelection() {
        checkGameOver();

        int[] is = {activeCards.indexOf(selection.getCard1()), activeCards.indexOf(selection.getCard2()), activeCards.indexOf(selection.getCard3())};
        int[] ir = replaceIndices(is);

        if (MainActivity.multiplayer) GameView.setByteReplacement(is[0], ir[0], is[1], ir[1], is[2], ir[2]); //send replacement data to all players
    }

    private int[] replaceIndices(int[] is) {
        //Give them a small shuffle so the guaranteed card isn't always the first one picked
        int r = random.nextInt(3);
        int[] ri = new int[3];
        ri[0] = replaceCards(is[0]);
        ri[1] = replaceCards(is[1]);
        ri[2] = replaceCards(is[2]);
        ri[r] = replaceGara(is[r]);

        return ri;
    }

    private void  checkGameOver() {
        //Log.d("Cardleft",""+availableCards.size());
        if (availableCards.size() == 3) {
            for (Card card: availableCards) {
                card.setImage();
            }
            activeCards.addAll(availableCards);
            availableCards.removeAll(availableCards);
            //Log.d("Game", "over");
            gameOver = true;
        }
    }

    public void reset() {
        availableCards.clear();
        activeCards.clear();
    }
}
