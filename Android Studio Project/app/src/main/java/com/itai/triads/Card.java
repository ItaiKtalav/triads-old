package com.itai.triads;

import android.content.Context;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;

public class Card {

    Context context;

    private int number;
    private int shape;
    private int colour;
    private int fill;
    private boolean selected; //Just use to decide background colour
    private MotionCard motionCard;

    private Bitmap image;

    //Constants
    private final int backroundColour = Color.WHITE;
    private final int selectedColour = Color.GRAY;

    private static int height = 300; //Width will be scaled according to the image dimensions

    public Card(Context context, int number, int shape, int colour, int fill) {
        this.context = context;

        this.number = number;
        this.shape = shape;
        this.colour = colour;
        this.fill = fill;
        this.selected = false;
    }

    public int getNumber() {
        return number;
    }
    public int getShape() {
        return shape;
    }
    public int getColour() {
        return colour;
    }
    public int getFill() {
        return fill;
    }
    public Bitmap getImage() {
        return image;
    }
    public MotionCard getMotionCard() { return motionCard; }
    public boolean isSelected() { return selected; }
    public static void resize(double ratio) { height *= ratio; }

    public int getWidth() {
        if (image != null) return image.getWidth();
        else return 300; //For testing. Arbitrary
    }
    public int getHeight() {
        if (image != null) return image.getHeight();
        else return 100; //For testing. Arbitrary
    }

    private Bitmap setSize(Bitmap image) {
        float ratio = (float) image.getHeight()/(float) image.getWidth();
        return Bitmap.createScaledBitmap(image, height, (int)(height*ratio), true);
    }
    /*
    Since each card is a transparent background, this code generates a solid background colour by making a box of that colour
    that is the same dimensions as the card and then overlays the card onto it
     */
    private Bitmap generateBackground(Bitmap image, int backroundColour) {
        Bitmap imageWithBackground = Bitmap.createBitmap(image.getWidth(), image.getHeight(), Bitmap.Config.ARGB_8888);
        Canvas canvas = new Canvas(imageWithBackground);

        Paint background = new Paint();
        background.setStyle(Paint.Style.FILL);
        background.setColor(backroundColour);
        canvas.drawPaint(background);

        canvas.drawBitmap(image,0,0, new Paint(Paint.FILTER_BITMAP_FLAG));
        return imageWithBackground;
    }
    private void setImage(int backroundColour) {
        Resources resources = context.getResources();
        int resourceId = resources.getIdentifier("c"+(number+1)+shape+colour+fill, "drawable", context.getPackageName());
        this.image = generateBackground(setSize(BitmapFactory.decodeResource(context.getResources(), resourceId)), backroundColour);
    }

    public void setImage() {
        setImage(backroundColour);
    }

    public boolean equals(Card other) {
        return number == other.getNumber() && shape == other.getShape() && colour == other.getColour() && fill == other.getFill();
    }

    public String toString() {
        return "Card: "+number+" "+shape+" "+colour+" "+fill;
    }

    public void touch(MotionCard motionCard) {
        if (!selected){
            select(motionCard);
        }
        else {
            deselect();
        }
    }

    public void select(MotionCard motionCard) {
        this.motionCard = motionCard;
        selected = true;
        if (image != null)setImage(selectedColour);
    }

    public void deselect(){
        motionCard = null;
        selected = false;
        if (image != null)setImage(backroundColour);
    }

}


