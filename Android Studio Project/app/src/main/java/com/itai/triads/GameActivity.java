package com.itai.triads;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.DisplayMetrics;
import android.widget.FrameLayout;
import android.widget.LinearLayout;

import com.google.android.gms.games.multiplayer.Participant;
import com.google.android.gms.games.multiplayer.realtime.Room;

import java.util.ArrayList;

public class GameActivity extends AppCompatActivity {

    private GameView gameSurfaceView;
    private static ArrayList<PlayerT> playerTags;

    private static Room room = null;
    private static String mMyParticipantId = null;
    public static void setRoom(Room room, String mMyParticipantId) {
        GameActivity.room = room;
        GameActivity.mMyParticipantId = mMyParticipantId;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_game);

        getSupportActionBar().hide();


        DisplayMetrics displayMetrics = new DisplayMetrics();
        getWindowManager().getDefaultDisplay().getMetrics(displayMetrics);
        int width = displayMetrics.widthPixels;

        if (width < 1080) {
            Card.resize(0.666);
            Board.getInstance().spaceRatio(0.666);
        }

        //Setup all player tags on the screen
        playerTags = new ArrayList<PlayerT>();
        PlayerT myTag = null;
        if (room != null) {
            for (int i = 0; i < room.getParticipants().size(); i++) {
                Participant participant = room.getParticipants().get(i);
                if (participant.getParticipantId().equals(mMyParticipantId)) {
                    myTag = new PlayerT(this, participant.getDisplayName(), (i + 1) * width / (room.getParticipants().size() + 1), 1000);
                    playerTags.add(myTag);
                } else {
                    playerTags.add(new PlayerT(this, participant.getDisplayName(), (i + 1) * width / (room.getParticipants().size() + 1), 1000));
                }
            }
        }
        else { //If it's single player, this isn't really needed but whatever
            myTag = new PlayerT(this, "Me", 500, 1000);
            playerTags.add(myTag);
        }

        //Create and get the game surface
        gameSurfaceView = new GameView(getApplicationContext(), myTag);

        //Made the surface the bottom view, so we can overlay things on it
        gameSurfaceView.setZOrderOnTop(false);


        //Setup our layout and add our game surface to it
        FrameLayout gameLayout = (FrameLayout) findViewById(R.id.game_activity);
        gameLayout.addView(gameSurfaceView);

        LinearLayout gameWidgets = new LinearLayout (this);

        for (PlayerT player : playerTags) {
            gameWidgets.addView(player);
        }

        gameLayout.addView(gameWidgets);
    }

    public static void addScore(byte[] message) {
        int playerIndex = (int)message[1];
        String displayName = room.getParticipants().get(playerIndex).getDisplayName();

        for (PlayerT playerTag : playerTags) {
            if (playerTag.getName().equals(displayName)) {
                playerTag.incrementScore();
                Board.getInstance().multiReplace(message, (int)playerTag.getX(), (int)playerTag.getY());
                return;
            }
        }

    }


    @Override
    protected void onResume() {
        super.onResume();
        gameSurfaceView.resume();
    }

    @Override
    protected void onPause() {
        super.onPause();
        gameSurfaceView.pause();
    }


}
