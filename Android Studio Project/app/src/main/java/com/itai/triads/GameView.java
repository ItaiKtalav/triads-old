package com.itai.triads;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.support.annotation.NonNull;
import android.util.Log;
import android.view.MotionEvent;
import android.view.SurfaceHolder;
import android.view.SurfaceView;

import com.google.android.gms.auth.api.signin.GoogleSignIn;
import com.google.android.gms.games.Games;
import com.google.android.gms.games.RealTimeMultiplayerClient;
import com.google.android.gms.games.multiplayer.realtime.Room;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;

import java.util.HashSet;

//This is the view that will sit on top of the layout
public class GameView extends SurfaceView implements SurfaceHolder.Callback, Runnable {

    private SurfaceHolder surfaceHolder = null;
    private Board board;
    private Context context;
    private Canvas canvas;
    private Thread gameThread;
    private boolean isPlaying;
    private long nextFrameTime;
    private PlayerT playerT;
    private boolean host;

    private static Room mRoom;
    private static String mMyParticipantId;
    private volatile static byte[] byteMessageReplacement;

    // Update the game 10 times per second
    private final long FPS = 10;
    // There are 1000 milliseconds in a second
    private final long MILLIS_PER_SECOND = 1000;

    public GameView(Context context, PlayerT playerT) {
        super(context);
        setFocusable(true);
        this.context = context;
        this.playerT = playerT;

        //Setup our surface holder
        surfaceHolder = getHolder();
        surfaceHolder.addCallback(this);

        //Setup board
        board = Board.getInstance();
        //See Board.java to find out why this is necessary
        board.setContext(context);
        board.setPlayerT(playerT);

        nextFrameTime = System.currentTimeMillis();

        Log.d("Stating game", ""+MainActivity.multiplayer);
        if (!MainActivity.multiplayer) {
            Board.getInstance().setupBoard();
        }
        else if (MainActivity.thisHost) { //If this is a multiplaer game and we are not the host, then we don't need to setup the board here as it will be done when the host sends the board setup message from the section of the code that catches the message
            Log.d("Multi setup", "attempting to send message");
            int[] c = Board.getInstance().setupBoard();
            byte[] message = new byte[13]; //TODO change to soft value

            message[0] = 'I';

            for (int i = 0; i < c.length; i++) {
                message[i + 1] = (byte) c[i];
            }
            sendToAllReliably(message);
        }

    }

    public static void setRoom(Room room, String id) {
        mRoom = room;
        mMyParticipantId = id;
    }

    public static void setByteReplacement(int i1, int i2, int i3, int i4, int i5, int i6) {
        byteMessageReplacement = new byte[] {'R', (byte)mRoom.getParticipantIds().indexOf(mMyParticipantId),
                (byte)i1, (byte)i2, (byte)i3, (byte)i4, (byte)i5, (byte)i6};
    }

    @Override
    public void run() {

        while (!board.isGameOver()) {
            update();
            drawGame();
        }

    }

    private void update() {
        if(nextFrameTime <= System.currentTimeMillis()){
            nextFrameTime = System.currentTimeMillis() + MILLIS_PER_SECOND / FPS;
        }

    }

    private void drawGame() {
        if (surfaceHolder.getSurface().isValid()) {
            canvas = surfaceHolder.lockCanvas();

            //background colour
            canvas.drawColor(Color.BLACK);

            board.drawBoard(canvas);

            MotionCard.handle(canvas);

            surfaceHolder.unlockCanvasAndPost(canvas);
        }
    }

    @Override
    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);
    }

    @Override
    public void surfaceCreated(SurfaceHolder surfaceHolder) {
        Canvas canvas = null;
        try {
            canvas = surfaceHolder.lockCanvas(null);

            synchronized (surfaceHolder) {
                draw(canvas);
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            if (canvas != null) {
                surfaceHolder.unlockCanvasAndPost(canvas);
            }
        }
    }

    @Override
    public void surfaceChanged(SurfaceHolder surfaceHolder, int i, int i1, int i2) {

    }

    @Override
    public void surfaceDestroyed(SurfaceHolder surfaceHolder) {

    }

    @Override
    public boolean onTouchEvent(MotionEvent event) {
        if (board.touch(event.getX(), event.getY()) && MainActivity.multiplayer) { //True if the touch found a valid triad
            sendToAllReliably(byteMessageReplacement);
        }
        return super.onTouchEvent(event);
    }

    public void pause() {
        isPlaying = false;
        try {
            gameThread.join();
        } catch (InterruptedException e) {
            // Error
        }
    }

    public void resume() {
        isPlaying = true;
        gameThread = new Thread(this);
        gameThread.start();
    }


    HashSet<Integer> pendingMessageSet = new HashSet<>();
    synchronized void recordMessageToken(int tokenId) {
        pendingMessageSet.add(tokenId);
    }
    void sendToAllReliably(byte[] message) {
        for (String participantId : mRoom.getParticipantIds()) {
            if (!participantId.equals(mMyParticipantId)) {
                Task<Integer> task = Games.
                        getRealTimeMultiplayerClient(context, GoogleSignIn.getLastSignedInAccount(context))
                        .sendReliableMessage(message, mRoom.getRoomId(), participantId,
                                handleMessageSentCallback).addOnCompleteListener(new OnCompleteListener<Integer>() {
                            @Override
                            public void onComplete(@NonNull Task<Integer> task) {
                                // Keep track of which messages are sent, if desired.
                                recordMessageToken(task.getResult());
                            }
                        });
                Log.d("MESSAGING", "Sending message:" + message);
            }
        }
    }

    private RealTimeMultiplayerClient.ReliableMessageSentCallback handleMessageSentCallback =
            new RealTimeMultiplayerClient.ReliableMessageSentCallback() {
                @Override
                public void onRealTimeMessageSent(int statusCode, int tokenId, String recipientId) {
                    // handle the message being sent.
                    synchronized (this) {
                        pendingMessageSet.remove(tokenId);
                    }
                }
            };

}
