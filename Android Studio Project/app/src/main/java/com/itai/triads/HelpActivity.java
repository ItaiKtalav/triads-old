package com.itai.triads;

import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.Random;

public class HelpActivity extends AppCompatActivity implements View.OnClickListener {

    Random random = new Random();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_help);

        ((Button) findViewById(R.id.validButton)).setOnClickListener(this);
        ((Button) findViewById(R.id.invalidButton)).setOnClickListener(this);
        ((Button) findViewById(R.id.randomButton)).setOnClickListener(this);

        show(randomValid());
    }

    @Override
    public void onClick(View v) {
        Log.d("click", "Have been clicked");
        switch (v.getId()) {
            case R.id.validButton:
                Log.d("Touch me", "hehe that tickles");
                show(randomValid());
                break;
            case R.id.invalidButton:
                show(randomInvalid());
                break;
            case R.id.randomButton:
                show(randomRandom());
                break;
        }
    }

    private void show(Card[] cards) {
        int resourceId;

        populateLabels(cards);

        ImageView[] exampleCardViews = { (ImageView) findViewById(R.id.exampleCard1),
                (ImageView) findViewById(R.id.exampleCard2),
                (ImageView) findViewById(R.id.exampleCard3)};

        for (int i = 0; i < 3; i++) {
            resourceId = getResources().getIdentifier("c"+(cards[i].getNumber()+1)+cards[i].getShape()+cards[i].getColour()+cards[i].getFill(), "drawable", getPackageName());
            exampleCardViews[i].setImageBitmap(BitmapFactory.decodeResource(getResources(), resourceId));
        }
    }

    private void populateLabels(Card[] shown) {
        ((TextView) findViewById(R.id.isValid)).setText("Valid");
        ((TextView) findViewById(R.id.numberSameDifferent)).setText(attributeStatus(shown[0].getNumber(), shown[1].getNumber(), shown[2].getNumber()));
        ((TextView) findViewById(R.id.shapeSameDifferent)).setText( attributeStatus(shown[0].getShape(),  shown[1].getShape(),  shown[2].getShape()));
        ((TextView) findViewById(R.id.colourSameDifferent)).setText(attributeStatus(shown[0].getColour(), shown[1].getColour(), shown[2].getColour()));
        ((TextView) findViewById(R.id.fillSameDifferent)).setText(  attributeStatus(shown[0].getFill(),   shown[1].getFill(),   shown[2].getFill()));
    }

    private String attributeStatus(int i1, int i2, int i3) {
        TextView isValid = (TextView) findViewById(R.id.isValid);

        if (i1 == i2) {
            if (i2 == i3) {
                return "All same";
            }
            else {
                isValid.setText("Invalid");
                return "Two same one different";
            }
        }
        else {
            if (i2 != i3) {
                if (i3 != i1) {
                    return "All different";
                }
                else {
                    isValid.setText("Invalid");
                    return "Two same one different";
                }
            }
            else {
                isValid.setText("Invalid");
                return "Two same one different";
            }
        }
    }

    private Card[] randomValid() {
        Card[] valid = new Card[3];

        //Two random cards
        valid[0] = new Card (null,random.nextInt(3), random.nextInt(3), random.nextInt(3), random.nextInt(3));
        valid[1] = new Card (null,random.nextInt(3), random.nextInt(3), random.nextInt(3), random.nextInt(3));

        //Make sure they're not the same card
        while (valid[0].equals(valid[1])) {
            valid[1] = new Card (null,random.nextInt(3), random.nextInt(3), random.nextInt(3), random.nextInt(3));
        }

        //The one that would make it a triad
        valid[2] = new Card (null,(3 - ((valid[0].getNumber()+valid[1].getNumber()) % 3)) % 3, (3 - ((valid[0].getShape()+valid[1].getShape()) % 3)) % 3, (3 - ((valid[0].getColour()+valid[1].getColour()) % 3)) % 3, (3 - ((valid[0].getFill()+valid[1].getFill()) % 3)) % 3);

        return valid;
    }

    private Card[] randomInvalid() {
        //Three random cards
        Card[] invalid = { new Card (null,random.nextInt(3), random.nextInt(3), random.nextInt(3), random.nextInt(3)),
                           new Card (null,random.nextInt(3), random.nextInt(3), random.nextInt(3), random.nextInt(3)),
                           new Card (null,random.nextInt(3), random.nextInt(3), random.nextInt(3), random.nextInt(3))
        };

        //Make sure none of the three are the same
        while (invalid[0].equals(invalid[1]) || invalid[1].equals(invalid[2])) {
            invalid[1] = new Card (null,random.nextInt(3), random.nextInt(3), random.nextInt(3), random.nextInt(3));
            invalid[2] = new Card (null,random.nextInt(3), random.nextInt(3), random.nextInt(3), random.nextInt(3));
        }

        //The one that would make it a triad
        Card valid = new Card (null,(3 - ((invalid[0].getNumber()+invalid[1].getNumber()) % 3)) % 3, (3 - ((invalid[0].getShape()+invalid[1].getShape()) % 3)) % 3, (3 - ((invalid[0].getColour()+invalid[1].getColour()) % 3)) % 3, (3 - ((invalid[0].getFill()+invalid[1].getFill()) % 3)) % 3);

        //Make sure the three cards we chose are not a valid triad:
        while (invalid[2].equals(valid)) {
            invalid[2] = new Card (null,random.nextInt(3), random.nextInt(3), random.nextInt(3), random.nextInt(3));
        }
        return invalid;
    }

    private Card[] randomRandom() {
        if (random.nextInt(2) == 1) { //50/50 chance
            return randomValid();
        }
        else {
            return randomInvalid();
        }
    }


}
