package com.itai.triads;

import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Paint;

import java.util.ArrayList;

public class MotionCard {

    private static ArrayList<MotionCard[]> motionCards = new ArrayList<MotionCard[]>();

    private int posX;
    private int posY;
    private int desX;
    private int desY;
    private final double speed = 0.96;
    private Bitmap image;

    public MotionCard(int posX, int posY, int desX, int desY, Bitmap image) {
        this.posX = posX;
        this.posY = posY;
        this.desX = desX;
        this.desY = desY;
        this.image = image;
    }

    public void move() {
        double deltaX = posX - desX;
        double deltaY = posY - desY;

        posX = (int)(desX + speed*deltaX);
        posY = (int)(desY + speed*deltaY);
    }

    public void draw(Canvas canvas) {
        canvas.drawBitmap(image, posX, posY, new Paint());
    }

    public boolean destinationReached() {
        double distX = Math.abs(posX-desX);
        double distY = Math.abs(posY-desY);
        return distX <= 50 || distY <= 50;
    }

    public static void addNew(MotionCard m1, MotionCard m2, MotionCard m3) {
        MotionCard[] newMotionCards = new MotionCard[3];
        newMotionCards[0] = m1;
        newMotionCards[1] = m2;
        newMotionCards[2] = m3;
        motionCards.add(newMotionCards);
    }
    public static void addNew(MotionCard[] newMotionCards) {
        if (newMotionCards.length == 3) { //Just for safety lol
            motionCards.add(newMotionCards);
        }
    }

    public static void handle(Canvas canvas) {
        for (MotionCard[] currentMotionCards : motionCards) {
            for (int i = 0; i < currentMotionCards.length; i++) {
                if (currentMotionCards[i] != null) {
                    if (!currentMotionCards[i].destinationReached()) {
                        currentMotionCards[i].move();
                        currentMotionCards[i].draw(canvas);
                    } else {
                        currentMotionCards[i] = null;
                    }
                }
            }
            if (currentMotionCards[0] == null &&currentMotionCards[1] == null && currentMotionCards[2] == null) {
                motionCards.remove(currentMotionCards);
            }
        }

    }


}
