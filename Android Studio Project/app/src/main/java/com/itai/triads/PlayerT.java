package com.itai.triads;

import android.content.Context;
import android.graphics.Color;
import android.support.v7.widget.AppCompatTextView;

public class PlayerT extends AppCompatTextView {
    private int score;
    private String name;

    public PlayerT(Context context, String name, float posX, float posY) {
        super(context);
        this.name = name;
        score = 0;
        setX(posX);
        setY(posY);
        setText(name+": "+score);
        setBackgroundColor(Color.WHITE);
    }

    public String getName() {
        return name;
    }

    public void incrementScore() {
        score += 3;
        setText(name+": "+score);
    }
}
