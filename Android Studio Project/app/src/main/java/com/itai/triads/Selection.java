package com.itai.triads;

public class Selection {
    private static final Selection ourInstance = new Selection();

    private Card card1 = null;
    private Card card2 = null;
    private Card card3 = null;

    public static Selection getInstance() {
        return ourInstance;
    }

    private Selection() {
    }

    public Card getCard1() {
        return card1;
    }
    public Card getCard2() {
        return card2;
    }
    public Card getCard3() {
        return card3;
    }

    public void removeCard(Card card) {
        if (card1 != null && card1.equals(card)) {
            card1 = null;
        }
        else if (card2 != null && card2.equals(card)) {
            card2 = null;
        }
        else if (card3!= null){
            card3 = null;
        }
    }

    public boolean contains(Card card) {
        return card1.equals(card) || card2.equals(card) || card3.equals(card);
    }

    public void addCard(Card card) {
        if (card1 == null) {
            card1 = card;
        }
        else if (card2 == null) {
            card2 = card;
        }
        else {
            card3 = card;
        }
    }

    public void deselectAll() {
        card1.deselect();
        card2.deselect();
        card3.deselect();
    }

    public void removeAll() {
        deselectAll();
        empty();
    }

    public void empty() {
        card1 = null;
        card2 = null;
        card3 = null;
    }

    public boolean isFull() {
        return card1 != null && card2 != null && card3 != null;
    }
    public boolean isEmpty() {
        return card1 == null && card2 == null && card3 == null;
    }

    public boolean isTriad() {
        //If you want to know why this works, the mathematical proof is in section 3 of this online PDF
        //SETs and Anti-SETs: The Math Behind the Game ofSET /// Charlotte Chan /// 19 July 2010
        //http://www-personal.umich.edu/~charchan/SET.pdf

        int o = 0;
        o += (card1.getNumber() + card2.getNumber() + card3.getNumber()) % 3;
        o += (card1.getShape()  + card2.getShape()  + card3.getShape())  % 3;
        o += (card1.getColour() + card2.getColour() + card3.getColour()) % 3;
        o += (card1.getFill()   + card2.getFill()   + card3.getFill())   % 3;

        return o == 0;
    }
}
