package com.itai.triads;

import org.junit.Test;

import java.util.ArrayList;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

public class GameTesting {

    /*
    We assume everything is blank and we don't need to reset anything.
     */
    Selection selection = Selection.getInstance();
    Board board = Board.getInstance();

    @Test
    public void validTriad() {
        selection.empty();

        Card card1 = new Card(null, 0, 0, 0,0);
        Card card2 = new Card(null, 1, 1, 1,1);
        Card card3 = new Card(null, 2, 2, 2,2);
        Card card4 = new Card(null, 2, 1, 0,0);
        Card card5 = new Card(null, 0, 1, 2,2);
        Card card6 = new Card(null, 2, 2, 0,0);
        Card card7 = new Card(null, 0, 0, 2,2);
        Card card8 = new Card(null, 2, 1, 2,2);
        Card card9 = new Card(null, 2, 0, 2,2);

        selection.addCard(card1);
        selection.addCard(card2);
        selection.addCard(card3);
        assertTrue(selection.isTriad());
        selection.empty();

        selection.addCard(card1);
        selection.addCard(card2);
        selection.addCard(card4);
        assertFalse(selection.isTriad());
        selection.empty();

        selection.addCard(card2);
        selection.addCard(card6);
        selection.addCard(card7);
        assertTrue(selection.isTriad());
        selection.empty();

        selection.addCard(card1);
        selection.addCard(card5);
        selection.addCard(card7);
        assertFalse(selection.isTriad());
        selection.empty();

        selection.addCard(card3);
        selection.addCard(card8);
        selection.addCard(card9);
        assertTrue(selection.isTriad());
        selection.empty();
    }

    @Test
    public void testCardEquals() {
        //Quick test just to make sure the next one is actually valid (and that I did't make a silly mistake)
        Card constant = new Card(null, 0, 0, 0, 0);
        assertTrue (new Card(null, 0, 0, 0, 0).equals(constant));
        assertFalse(new Card(null, 1, 0, 0, 0).equals(constant));
        assertFalse(new Card(null, 0, 1, 0, 0).equals(constant));
        assertFalse(new Card(null, 0, 0, 1, 0).equals(constant));
        assertFalse(new Card(null, 0, 0, 0, 1).equals(constant));
    }

    @Test
    public void testCardsGeneration() {
        board.reset();
        //This will call the method that generates the cards
        board.setContext(null);
        ArrayList<Card> cards = board.getAvailableCards();
        assertEquals(cards.size(), 81);

        //Now to make sure that all of them are unique
        assertTrue(allUnique(cards));
    }

    /*
    Simulates a non-random game we can test
     */
    @Test
    public void simulateTouch() {
        board.reset();
        board.setContext(null);
        board.setupBoard(new byte[] {'N', 0, 0, 0, 13, 6, 45, 12, 67, 66, 10, 24, 33});
        board.touch(board.getOffsetX()+5+board.getSpace()  +300,   board.getOffsetY()+5);
        board.touch(board.getOffsetX()+5+board.getSpace()*2+600, board.getOffsetY()+5);
        assertTrue(board.touch(board.getOffsetX()+5, board.getOffsetY()+5));

        board.reset();
        board.setContext(null);
        board.setupBoard(new byte[] {'N', 0, 3, 7, 13, 6, 45, 12, 67, 66, 10, 24, 33});
        board.touch(board.getOffsetX()+5+board.getSpace()  +300,   board.getOffsetY()+5);
        board.touch(board.getOffsetX()+5+board.getSpace()*2+600, board.getOffsetY()+5);
        assertFalse(board.touch(board.getOffsetX()+5, board.getOffsetY()+5));
    }

    private static boolean allUnique(ArrayList<Card> a) {
        for (int i = 0; i < a.size() - 1; i++) {
            for (int j = i + 1; j < a.size(); j++) {
                if (a.get(i).equals(a.get(j))) {
                    return false;
                }
            }
        }
        return true;
    }


}