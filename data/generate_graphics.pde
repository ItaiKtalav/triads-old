
//0 = red, 1 = orange, 2 = blue
final int[] colours     = {color(255, 0, 0),     color(0, 190, 0),   color(0, 0, 255)};
final int[] fillColours = {color(255, 127, 127), color(180, 255, 180), color(0, 127, 255)};
final int shapeSize = 80;
final int seperation = 16;
final int backgroundColour = -1;
final String filePrefix = "c";

void setup() {
  size(330, 160);
  background(backgroundColour);
  strokeWeight(5);
  
  generateAll(1, 0, 0, 0);
  //drawCard(3, 2, 1, 1); //test
}

void generateAll(int number, int shape, int colour, int filling) {
  
  if (number == 4 || shape == 3 || colour == 3 || filling == 3) {
    return;
  }
  
  generateAll(number+1, shape,   colour,   filling);
  generateAll(number,   shape+1, colour,   filling);
  generateAll(number,   shape,   colour+1, filling);
  generateAll(number,   shape,   colour,   filling+1);
  
  drawCard(number, shape, colour, filling);
  saveTransparentCanvas(backgroundColour, filePrefix + number + shape + colour + filling);
  background(backgroundColour); //clear the canvas or else trippy stuff happens
}

void drawCard(int number, int shape, int colour, int filling) {
  for (int i = 0; i < number; i++) {
    drawShape(((width/(number + 1)+seperation) * (i + 1))-seperation*(number+1)/2, height/2, shape, colour, filling);
  }
}

void drawShape(int px, int py, int shape, int colour, int filling) {
  //Shapes:   0 = rectangle, 1 = circle, 2 = triangle
  //Fillings: 0 = full, 1 = partial, 2 = open
  
  stroke(colours[colour]);
  
  if (filling == 0) {
    fill(colours[colour]);
  }
  else if (filling ==1) {
    fill(fillColours[colour]);
  }
  else {
    noFill();
  }
  
  if (shape == 0) { //rect
    rect(px - shapeSize/2, py - shapeSize/4 * 3, shapeSize, shapeSize/4 * 6);
  }
  else if (shape == 1) { //circle
    ellipse(px, py, shapeSize, shapeSize);
  }
  else { //triangle
    triangle(px,               py - shapeSize/2, 
             px - shapeSize/2, py + shapeSize/2, 
             px + shapeSize/2, py + shapeSize/2);
  }
  
}

//Code from https://forum.processing.org/two/discussion/12036/saving-sketch-with-a-transparent-background
//Thanks GoToLoop :)
void saveTransparentCanvas(final color bg, final String name) {
  final PImage canvas = get();
  canvas.format = ARGB;
 
  final color p[] = canvas.pixels, bgt = bg & ~#000000;
  for (int i = 0; i != p.length; ++i)  if (p[i] == bg)  p[i] = bgt;
 
  canvas.updatePixels();
  canvas.save(dataPath(name+".png"));
}
